let spawned_car = [];
let cars = [];
let min_cars = 1;
let spawned_car_index = 1;
let max_cars = 12;
let train_speed = 2;

function spawn_car(spawn_point) {
    if (spawned_car.length > 0 && spawned_car_index == max_cars) {
        let index = Math.floor(Math.random()*max_cars)+0;
        if(index == 11 || index == 12 || (index == 10 && spawn_point == 12)) index = Math.floor(Math.random()*9)+0;
        if(spawn_point == 12) index = 10;
        if(spawn_point == 13 || spawn_point == 14) index = 11;
        let new_car = spawned_car[(index)].clone();
        scene.add(new_car);
        new_car.scale.x = 2;
        new_car.scale.y = 2;
        new_car.scale.z = 2;
        new_car.waypoint = 0;
        new_car.speed = max_speed;
        new_car.road = spawn_point;

        new_car.stop = false;
        new_car.type = "car";
        new_car.subtype = "regular";
        new_car.visible = true;

        if(index == 10) new_car.subtype = "bus";
        if(index == 11 || index == 12) new_car.subtype = "train";
        if(spawn_point == 13 || spawn_point == 14) new_car.speed = train_speed;
    
        new_car.position.x = waypoints_cars[new_car.road][0];
        new_car.position.z = waypoints_cars[new_car.road][1];

        cars.push(new_car);
        traffic.push(new_car);
    }
}

function spawn_car_init() {
    let car;
    let loadingManager = new THREE.LoadingManager( function() {
        let refObject = window.referenceModel;
        let clone = new THREE.Mesh( refObject.geometry, refObject.material );
    });

    if (spawned_car.length <= max_cars) {
        let loader = new THREE.ColladaLoader( loadingManager );
        loader.load( 'models/car_'+spawned_car_index+'.dae', function ( collada ) {
            car = collada.scene;
            
            window.referenceModel = collada.scene.children[0];
            spawned_car.push(car);
            if(spawned_car_index < max_cars) spawned_car_index++;
            spawn_car_init();
        });
    }
}

let waypoint_setter = [];