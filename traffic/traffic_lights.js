let traffic_lights = { 
    A1: { 
        state: "red",
        road_index: [6],
        current_traffic: 0,
        lights: []
    },
    A2: { 
        state: "red",
        road_index: [5],
        waypoint_index_of_light: [0],
        current_traffic: 0,
        lights: []
    },
    A3: { 
        state: "red",
        road_index: [4, 9],
        current_traffic: 0,
        lights: []
    },
    A4: { 
        state: "red",
        road_index: [7],
        current_traffic: 0,
        lights: []
    },
    A5: { 
        state: "red",
        road_index: [0],
        current_traffic: 0,
        lights: []
    },
    A6: { 
        state: "red",
        road_index: [10, 11],
        current_traffic: 0,
        lights: []

    },
    A7: { 
        state: "red",
        road_index: [8],
        current_traffic: 0,
        lights: []
    },
    A8: { 
        state: "red",
        road_index: [3],
        waypoint_index_of_light: [0],
        current_traffic: 0,
        lights: []
    },
    A9: { 
        state: "red",
        road_index: [2],
        current_traffic: 0,
        lights: []
    },
    A10: { 
        state: "red",
        road_index: [1],
        current_traffic: 0,
        lights: []
    },
    B1: { 
        state: "red",
        road_index: [7,8],
        current_traffic: 0,
        lights: []
    },
    B2: { 
        state: "red",
        road_index: [6],
        waypoint_index_of_light: [1, 2],
        current_traffic: 0,
        lights: []
    },
    B3: { 
        state: "red",
        road_index: [4, 5],
        current_traffic: 0,
        lights: []
    },
    "C1.1": { 
        state: "red",
        road_index: [8, 9],
        waypoint_index_of_light: [1, 0],
        current_traffic: 0,
        lights: []
    },
    "C1.2": { 
        state: "red",
        road_index: [8, 9],
        waypoint_index_of_light: [0, 1],
        current_traffic: 0,
        lights: []
    },
    "C2.1": { 
        state: "red",
        road_index: [10, 6],
        waypoint_index_of_light: [0, 2],
        current_traffic: 0,
        lights: []
    },
    "C2.2": { 
        state: "red",
        road_index: [6, 10],
        waypoint_index_of_light: [1, 1],
        current_traffic: 0,
        lights: []
    },
    "C3.1": { 
        state: "red",
        road_index: [7, 11],
        waypoint_index_of_light: [0, 1],
        current_traffic: 0,
        lights: []
    },
    "C3.2": { 
        state: "red",
        road_index: [7, 11],
        waypoint_index_of_light: [1, 0],
        current_traffic: 0,
        lights: []
    },
    D1: { 
        state: "red",
        road_index: [12],
        current_traffic: 0,
        lights: []
    },
    E1: { 
        state: "red",
        road_index: [/*cars*/ [3, 5], /*cyclists*/ [0, 3, 6], /*pedestrians*/[4, 5, 6, 10]],
        waypoint_index_of_light: [[1,1], [0, 0, 0], [0, 0, 0, 2]],
        current_traffic: 0,
        lights: [[], [], []],
    },
    F1: {
        state: "red",
        road_index: [13],
        current_traffic: 0,
        lights: []
    },
    F2: {
        state: "red",
        road_index: [14],
        current_traffic: 0,
        lights: []
    }
};