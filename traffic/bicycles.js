let spawned_cyclist = [];
let cyclist = [];
let min_cyclists = 1;
let spawned_cyclist_index = 1;
let max_cyclists = 8;
let max_offset = 5;

function spawn_cyclist(spawn_point) {
    if (spawned_cyclist.length > 0 && spawned_cyclist_index == max_cyclists) {
        let group = new THREE.Group();
        scene.add(group);

        let cyclist_index = (Math.floor(Math.random()*max_cyclists)+0);
        let new_cyclist = spawned_cyclist[cyclist_index].clone();
        group.add(new_cyclist);
        scene.add(group);

        let max_speed_cyclist = Math.round((Math.random() * (1 - 0.4) + 0.4)*10)/10;
        if(cyclist_index == 3) max_speed_cyclist = 0.25;
        let offset_x = Math.round(Math.random() * (max_offset - -max_offset) + -max_offset);
        let offset_y = Math.round(Math.random() * (max_offset - -max_offset) + -max_offset);

        new_cyclist.scale.x = 2.2;
        new_cyclist.scale.y = 2.2;
        new_cyclist.scale.z = 2.2;
        new_cyclist.waypoint = 0;
        new_cyclist.speed = max_speed_cyclist;
        new_cyclist.road = spawn_point;
        new_cyclist.stop = false;
        new_cyclist.type = "cyclist";
        new_cyclist.offset_x = offset_x;
        new_cyclist.offset_y = offset_y;
        new_cyclist.max_speed = max_speed_cyclist;

        group.position.x = offset_x;
        group.position.z = offset_y;

        new_cyclist.position.x = waypoints_cyclists[new_cyclist.road][0];
        new_cyclist.position.z = waypoints_cyclists[new_cyclist.road][1];

        cyclist.push(new_cyclist);
        traffic.push(new_cyclist);
    }
}

function spawn_cyclist_init() {
    let cyclist;
    let loadingManager = new THREE.LoadingManager( function() {
        let refObject = window.referenceModel;
        let clone = new THREE.Mesh( refObject.geometry, refObject.material );
    });

    if (spawned_cyclist.length <= max_cyclists) {
        let loader = new THREE.ColladaLoader( loadingManager );
        loader.load( 'models/bicycle_'+spawned_cyclist_index+'.dae', function ( collada ) {
            cyclist = collada.scene;

            window.referenceModel = collada.scene.children[0];
            spawned_cyclist.push(cyclist);
            if(spawned_cyclist_index < max_cyclists) spawned_cyclist_index++;
            spawn_cyclist_init();
        });
    }
}