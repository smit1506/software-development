let speed_multiplier = 1;
const max_distance = 65;
const max_distance_lights = 50;
const min_speed = 0.02;
let max_speed = 1.5;
let decel_speed = 0.0415;
let accel_speed = 0.02;
let lights = [];
let traffic = [];

let active_red_lights_roads_pedestrians = [[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1]];
let active_red_lights_roads_cars = [[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1]];
let active_red_lights_roads_cyclists = [[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1],[-1]];

function fill_traffic() {
    spawn_car(Math.round(Math.random()*(waypoints_cars.length-3)));
    spawn_cyclist(Math.round(Math.random()*(waypoints_cyclists.length-1)));
    spawn_pedestrian(Math.round(Math.random()*(waypoints_pedestrians.length-1)));
}

function show_stop_waypoints(waypoints_indexof_lights,waypoints,type) {
    //Stop points/lights
    for(let i = 0; i < waypoints_indexof_lights.length; i++) {
        for(let x = 0; x < waypoints_indexof_lights[i].length; x++) {
            if(waypoints_indexof_lights[i][x] == -1) break;
            let size = 1.5;
            let position_y = 10;
            if (type == "car") {
                size = 3;
                position_y = 25;
            }

            let geometry = new THREE.SphereGeometry(size, 10, 10, 0, Math.PI * 2, 0, Math.PI * 2);
            let material = new THREE.MeshLambertMaterial();
            let sphere = new THREE.Mesh(geometry, material);

            sphere.position.x = waypoints[i][waypoints_indexof_lights[i][x]-1][0];
            sphere.position.z = waypoints[i][waypoints_indexof_lights[i][x]-1][1];
            sphere.position.y = position_y;

            sphere.road = i;
            sphere.index = x;
            sphere.type = type;
            sphere.stage = 0;
            if((i == 3 || i == 5) && type == "car") {
                sphere.name = "E1";
            }
            sphere.holders = [];
            
            
            scene.add(sphere);
            //lights.push(sphere);


            Object.keys(traffic_lights).forEach((key)=>{
                if (key[0] != 'E'){
                    if (
                        ((key[0] == "A" || key[0] == "D" || key[0] == "F") && sphere.type != "car") ||
                        (key[0] == "B" && sphere.type != "cyclist") ||
                        (key[0] == "C" && sphere.type != "pedestrian")
                    ){
                        return;
                    }
                    let road_index = traffic_lights[key].road_index.indexOf(sphere.road);
                    if (road_index > -1)
                        if (traffic_lights[key].hasOwnProperty("waypoint_index_of_light"))
                            push_light(traffic_lights[key].lights, traffic_lights[key].waypoint_index_of_light, sphere, road_index);
                        else
                            traffic_lights[key].lights.push(sphere);
                } else {
                    let index_of_road = -1;
                    let array_index = -1;
                    let road_index = traffic_lights[key].road_index;
                    switch (type) {
                        case "car":
                            index_of_road = road_index[0].indexOf(sphere.road);
                            array_index = 0;
                            break;
                        case "cyclist":
                            index_of_road = road_index[1].indexOf(sphere.road);
                            array_index = 1;
                            break;
                        case "pedestrian":
                            index_of_road = road_index[2].indexOf(sphere.road);
                            array_index = 2;
                        default:
                            break;
                    }
                    
                    if (index_of_road > -1){
                        if (traffic_lights[key].waypoint_index_of_light[array_index] != null)
                            push_light(traffic_lights[key].lights[array_index], traffic_lights[key].waypoint_index_of_light[array_index], sphere, index_of_road);
                        else
                            traffic_lights[key].lights[array_index].push(sphere);
                    }
                }
            });
        }
    }
}

function manage_traffic_lights() {
    for(let k = 0; k < traffic.length; k++) {
        let vehicle = traffic[k];
        let distance_x, distance_z, distance;
        for (const name in traffic_lights) {
            let road_idx = traffic_lights[name].road_index;
            if(!road_idx.includes(vehicle.road) && name == "E1" && !road_idx[0].includes(vehicle.road) && !road_idx[1].includes(vehicle.road) && !road_idx[2].includes(vehicle.road)) continue;

            traffic_lights[name].lights.forEach(light => {
                //E1....
                if(light != null && light.length > 1 && name == "E1") {
                    light.forEach(inner_light => {
                        if(inner_light != null && (traffic_lights[name].state == "red" || traffic_lights[name].state == "orange")) {
                            distance_x = Math.abs(inner_light.position.x - vehicle.position.x);
                            distance_z = Math.abs(inner_light.position.z - vehicle.position.z);
                            distance = Math.sqrt(distance_x*distance_x + distance_z*distance_z);
                            if (distance < 10) brake_vehicle(distance,10,vehicle);
                        }
                    });
                //The rest
                } else if(light != null && (traffic_lights[name].state == "red" || traffic_lights[name].state == "orange")) {
                    distance_x = Math.abs(light.position.x - vehicle.position.x);
                    distance_z = Math.abs(light.position.z - vehicle.position.z);
                    distance = Math.sqrt(distance_x*distance_x + distance_z*distance_z);
                    if (distance < 10 && light.road == vehicle.road) {
                        if(name != "F1" && name != "F2") traffic_lights[name].current_traffic = 1;
                        brake_vehicle(distance,10,vehicle);
                    }
                } else if (light != null && traffic_lights[name].state == "green" || traffic_lights[name].state == "orange" && vehicle.type == "car") {
                    vehicle.stop = false;
                }
            });
        }
    }
}


function animate_traffic() {
    manage_traffic_lights();

    for(let i = 0; i < traffic.length; i++) {
        if (traffic.indexOf(traffic[i]) > -1) {
            let vehicle = traffic[i];

            if(vehicle.type == "car") {
                target_waypoint = waypoints_cars[vehicle.road][vehicle.waypoint];
                vehicle.lookAt(target_waypoint[0],0,target_waypoint[1]);
            } else if (vehicle.type == "cyclist") {
                target_waypoint = waypoints_cyclists[vehicle.road][vehicle.waypoint];
                vehicle.lookAt(target_waypoint[0]+vehicle.offset_x,0,target_waypoint[1]+vehicle.offset_y);
            } else if (vehicle.type == "pedestrian") {
                target_waypoint = waypoints_pedestrians[vehicle.road][vehicle.waypoint];
                vehicle.lookAt(target_waypoint[0]+vehicle.offset_x,0,target_waypoint[1]+vehicle.offset_y);
            }

            if(!vehicle.stop && vehicle.speed < max_speed && vehicle.type == "car") vehicle.speed += accel_speed;
            if(!vehicle.stop && vehicle.speed < (vehicle.max_speed*speed_multiplier) && (vehicle.type == "cyclist" || vehicle.type == "pedestrian")) vehicle.speed += accel_speed;
            if(vehicle.speed <= 0) vehicle.speed = 0;

            //collision detection
            function collision_detection(other_vehicle) {
                if (other_vehicle.type == vehicle.type) {
                    let distance_x = Math.abs(other_vehicle.position.x - vehicle.position.x);
                    let distance_z = Math.abs(other_vehicle.position.z - vehicle.position.z);
                    let distance = Math.sqrt(distance_x*distance_x + distance_z*distance_z);

                    if(distance < 40 && other_vehicle.subtype != "bus") {
                        brake_vehicle(distance,29,vehicle);
                        vehicle.stop = true;
                    } else if (distance < max_distance*1.5 && other_vehicle.subtype == "bus") {
                        brake_vehicle(distance,55,vehicle);
                        vehicle.stop = true;
                    } else {
                        //if (vehicle.speed < max_speed) vehicle.speed += accel_speed;
                        if (vehicle.speed >= max_speed && vehicle.type == "car") vehicle.speed = max_speed;
                        vehicle.stop = false;
                    }
                }
            }

            for(let x = 0; x < traffic.length; x++) {
                let other_vehicle = traffic[x];
                if(vehicle.type == "car" && (other_vehicle.road == vehicle.road 
                    || ((other_vehicle.road == 3 && vehicle.road == 5) || (other_vehicle.road == 5 && vehicle.road == 3))
                    || ((other_vehicle.road == 10 && vehicle.road == 11) || (other_vehicle.road == 11 && vehicle.road == 10))) 
                    && traffic.indexOf(other_vehicle) < traffic.indexOf(vehicle) 
                    && other_vehicle.uuid != vehicle.uuid) {
                    collision_detection(other_vehicle);
                }
            }
            
            let speed = vehicle.speed;
            vehicle.speed = speed;

            let delta_x = target_waypoint[0] - vehicle.position.x;
            let delta_z = target_waypoint[1] - vehicle.position.z;
            let dist = Math.sqrt(delta_x*delta_x + delta_z*delta_z);
            if (dist > speed) {
                ratio = speed / dist;
                vehicle.position.x += ratio*delta_x;
                vehicle.position.z += ratio*delta_z;
                if(vehicle.type == "cyclist") {
                    if(frame % 15*vehicle.speed == 0 && vehicle.speed > 0.3) {
                        vehicle.scale.x = -vehicle.scale.x;
                    }
                } else if (vehicle.type == "pedestrian") {
                    if(frame % 10 == 0 && vehicle.speed > 0.075) {
                        vehicle.scale.x = -vehicle.scale.x;
                    }
                }
            } else {
                vehicle.position.x = target_waypoint[0];
                vehicle.position.z = target_waypoint[1];
            }

            if(vehicle.position.x == target_waypoint[0] && vehicle.position.z == target_waypoint[1] && vehicle.type == "car") {
                if(vehicle.waypoint == 0) {
                    vehicle.visible = false;
                } else {
                    vehicle.visible = true;
                }
                if(vehicle.waypoint < waypoints_cars[vehicle.road].length-1) {
                    vehicle.waypoint++;
                } else {
                    traffic.splice(traffic.indexOf(vehicle),1);
                    scene.remove(vehicle);
                }
            }else if (Math.round(vehicle.position.x) == target_waypoint[0] && Math.round(vehicle.position.z) == target_waypoint[1] && vehicle.type == "cyclist") {
                if(vehicle.waypoint < waypoints_cyclists[vehicle.road].length-1) {
                    vehicle.waypoint++;
                } else {
                    traffic.splice(traffic.indexOf(vehicle),1);
                    scene.remove(vehicle.parent);
                }
            } else if (Math.round(vehicle.position.x) == target_waypoint[0] && Math.round(vehicle.position.z) == target_waypoint[1] && vehicle.type == "pedestrian") {
                if(vehicle.waypoint < waypoints_pedestrians[vehicle.road].length-1) {
                    vehicle.waypoint++;
                } else {
                    traffic.splice(traffic.indexOf(vehicle),1);
                    scene.remove(vehicle.parent);
                }
            }
        }
    }
}

function brake_vehicle(distance,min_distance,vehicle) {
    if (vehicle.speed > min_speed) vehicle.speed -= accel_speed;
    if (vehicle.type != "car") min_distance = 3;
    if (distance <= min_distance) {
        vehicle.stop = true;
        vehicle.speed = -accel_speed;
    } 
    
}

function push_light(property_lights, waypoint_index_of_light, sphere, index){
    let waypoint_index = waypoint_index_of_light[index];
    if (waypoint_index in property_lights){
        if (property_lights[waypoint_index] == null){
            property_lights[waypoint_index] = sphere;
        }else if (property_lights[waypoint_index].road != sphere.road){
            if (waypoint_index == 0 || (waypoint_index > 0 && property_lights[property_lights.length-waypoint_index] == null) && !(lights.includes(sphere) && sphere.type == "pedestrian")){
                if (property_lights[(property_lights.length-waypoint_index)-1].road != sphere.road){
                    property_lights.push(sphere);
                    sphere.holders.push(property_lights);
                    lights.push(sphere);
                    //clean_array(property_lights);
                }
            }
            else
                property_lights.push(null);
        } else if (waypoint_index_of_light.length > index+1) {
            push_light(property_lights, waypoint_index_of_light, sphere, index+1);
        }
    }
    else{
        if (waypoint_index == property_lights.length && !(lights.includes(sphere) && sphere.type == "pedestrian")) {
            property_lights.push(sphere);
            sphere.holders.push(property_lights);            
            //clean_array(property_lights);

        }
        else
            property_lights.push(null);
    } 
}

function clean_array(array){
    for (let index = 0; index < array.length; index++) {
        if (array[index] == null){
            array.splice(index, 1);
        }
    }
}
