let spawned_pedestrian = [];
let pedestrian = [];
let min_pedestrians = 1;
let spawned_pedestrian_index = 1;
let max_pedestrians = 4;

function spawn_pedestrian(spawn_point) {
    if (spawned_pedestrian.length > 0 && spawned_pedestrian_index == max_pedestrians) {
        let group = new THREE.Group();
        scene.add(group);

        let pedestrian_index = (Math.floor(Math.random()*max_pedestrians)+0);
        let new_pedestrian = spawned_pedestrian[pedestrian_index].clone();
        group.add(new_pedestrian);
        scene.add(group);

        let max_speed_pedestrian = Math.round((Math.random() * (0.5 - 0.2) + 0.2)*10)/10;
        let offset_x = Math.round(Math.random() * (max_offset - -max_offset) + -max_offset);
        let offset_y = Math.round(Math.random() * (max_offset - -max_offset) + -max_offset);

        new_pedestrian.scale.x = 2;
        new_pedestrian.scale.y = Math.round((Math.random() * (2.8 - 2) + 2)*10)/10;
        new_pedestrian.scale.z = 2;
        new_pedestrian.waypoint = 0;
        new_pedestrian.speed = max_speed_pedestrian;
        new_pedestrian.road = spawn_point;
        new_pedestrian.stop = false;
        new_pedestrian.type = "pedestrian";
        new_pedestrian.offset_x = offset_x;
        new_pedestrian.offset_y = offset_y;
        new_pedestrian.max_speed = max_speed_pedestrian;

        group.position.x = offset_x;
        group.position.z = offset_y;

        new_pedestrian.position.x = waypoints_pedestrians[new_pedestrian.road][0];
        new_pedestrian.position.z = waypoints_pedestrians[new_pedestrian.road][1];

        pedestrian.push(new_pedestrian);
        traffic.push(new_pedestrian);
    }
}

function spawn_pedestrian_init() {
    let pedestrian;
    let loadingManager = new THREE.LoadingManager( function() {
        let refObject = window.referenceModel;
        let clone = new THREE.Mesh( refObject.geometry, refObject.material );
    });

    if (spawned_pedestrian.length <= max_pedestrians) {
        let loader = new THREE.ColladaLoader( loadingManager );
        loader.load( 'models/pedestrian_'+spawned_pedestrian_index+'.dae', function ( collada ) {
            pedestrian = collada.scene;

            window.referenceModel = collada.scene.children[0];
            spawned_pedestrian.push(pedestrian);
            if(spawned_pedestrian_index < max_pedestrians) spawned_pedestrian_index++;
            spawn_pedestrian_init();
        });
    }
}