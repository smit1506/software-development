function control_traffic_lights(data){
    data.forEach(entry => {
        let traffic_light = traffic_lights[entry.light];
        let active_red_lights_roads, waypoints_indexof_lights;
        let train_coming = false;

        switch (entry.light[0]) {
            case "A":
            case "F":
            case "D":
                active_red_lights_roads = active_red_lights_roads_cars;
                waypoints_indexof_lights = waypoints_indexof_lights_cars;
                break;
            case "B":
                active_red_lights_roads = active_red_lights_roads_cyclists;
                waypoints_indexof_lights = waypoints_indexof_lights_cyclists;
                break;
            case "C":
                active_red_lights_roads = active_red_lights_roads_pedestrians;
                waypoints_indexof_lights = waypoints_indexof_lights_pedestrians;
                break;
            case "E":
                active_red_lights_roads = [active_red_lights_roads_cars, active_red_lights_roads_cyclists, active_red_lights_roads_pedestrians];
                waypoints_indexof_lights = [waypoints_indexof_lights_cars, waypoints_indexof_lights_cyclists, waypoints_indexof_lights_pedestrians];
                train_coming = true;
                break;
            default:
                break;
        }

        traffic_light.state = entry.status;
        
        change_traffic_light_color(traffic_light.lights, traffic_light.state);
        let waypoint_index_of_light = (traffic_light.hasOwnProperty("waypoint_index_of_light")?traffic_light.waypoint_index_of_light:null);
        if (train_coming){
            for (let index = 0; index < traffic_light.road_index.length; index++) {
                if (waypoint_index_of_light != null)
                    set_traffic_lights(traffic_light, traffic_light.road_index[index], active_red_lights_roads[index], waypoints_indexof_lights[index], waypoint_index_of_light[index], entry.light);
                else    
                    set_traffic_lights(traffic_light, traffic_light.road_index[index], active_red_lights_roads[index], waypoints_indexof_lights[index], null, entry.light);
               

                    // if (traffic_light.state == "green"){
                //     console.log(active_red_lights_roads[index][traffic_light.road_index[index]]);
                //     active_red_lights_roads[index][traffic_light.road_index[index]] = [-1];
                // }

            }
        }else {
            if (traffic_light.hasOwnProperty("waypoint_index_of_light") && traffic_light.waypoint_index_of_light[0] == 1 &&traffic_light.waypoint_index_of_light[1] == 2 && traffic_light.road_index[0] == 6){
                //console.log("test");
            }
            set_traffic_lights(traffic_light, traffic_light.road_index, active_red_lights_roads, waypoints_indexof_lights, waypoint_index_of_light, entry.light);
        }

        
        //console.log(active_red_lights_roads_cars);
        //console.log(active_red_lights_roads_cyclists);
        //console.log(active_red_lights_roads_pedestrians);
    });
}
function set_traffic_lights(traffic_light, road_indexes, active_red_lights_roads, waypoints_indexof_lights, waypoint_index_of_light = null, name = null){
    let index;
    function toggle_light(light_index, current_lights, state){
        let output;
        if (waypoint_index_of_light[index] == light_index){
            output = (state != "green" ? light_index : -1);
            if(road_indexes.length < waypoint_index_of_light.length) index++;
        }
        else {
            output = (current_lights[light_index]>-1?current_lights[light_index]:-1);
        }
        return output;
    }
    road_indexes.forEach((road_index, idx) => {
        index = idx;
            // turn road with road_index to green
            if (!waypoint_index_of_light && waypoints_indexof_lights && road_index > -1){
                if (traffic_light.state == "green"){
                    active_red_lights_roads[road_index] = [-1];
                }else{
                    active_red_lights_roads[road_index] = [...Array(waypoints_indexof_lights[road_index].length).keys()];
                }
            }
            else if (waypoints_indexof_lights && road_index > -1){
                active_red_lights_roads[road_index] = waypoints_indexof_lights[road_index].map(
                    (_, light_index) => toggle_light(light_index, active_red_lights_roads[road_index], traffic_light.state)
                );
                
            }
            if (traffic_light.state == "green"){
                traffic_light.current_traffic = 0;
            }
            if (name != null && road_index>-1 && !active_red_lights_roads[road_index].hasOwnProperty("name") && active_red_lights_roads[road_index].name != ""){
                active_red_lights_roads[road_index].name = name;
            }
            else {

            }
        });
}

function change_traffic_light_color(lights, state){
    lights.flat().forEach((light)=>{
        if (light == null){
            return;
        }
        switch (state) {
            case "red":
                light.material.color.setHex(0xFF0000);
                break;
            case "orange":    
                light.material.color.setHex(0xFF8000);
                break;
            case "green":
                light.material.color.setHex(0x68f442);
            default:
                break;
        }
    });
    
    
}