import asyncio
import websockets
from threading import Timer
import datetime
import json
import time
import ast
import socket

port = 6969

bus_timer = 4.0
train_timer = 10.0
orange_timer = 4.0
green_timer = 10.0

traffic_lights = {}
action_list_all_red = [{"light": "F2", "status":"red", "timer": 0},{"light": "F1", "status":"red", "timer": 0},{"light": "D1", "status":"red", "timer": 0},{"light":"A1","status":"red","timer":0},{"light":"A2","status":"red","timer":0},{"light":"A3","status":"red","timer":0},{"light":"A4","status":"red","timer":0},{"light":"A5","status":"red","timer":0},{"light":"A6","status":"red","timer":0},{"light":"A7","status":"red","timer":0},{"light":"A8","status":"red","timer":0},{"light":"A9","status":"red","timer":0},{"light":"A10","status":"red","timer":0},{"light":"B1","status":"red","timer":0},{"light":"B2","status":"red","timer":0},{"light":"B3","status":"red","timer":0},{"light":"C1.1","status":"red","timer":0},{"light":"C1.2","status":"red","timer":0},{"light":"C2.1","status":"red","timer":0},{"light":"C2.2","status":"red","timer":0},{"light":"C3.1","status":"red","timer":0},{"light":"C3.2","status":"red","timer":0}]
waiting_priority_list = []
action_list = []

print(socket.gethostbyname(socket.gethostname()) + ":" + str(port))
msg = 'Send and receive byte arrays?'
send_byte_arrays = input("%s (y/N) " % msg).lower() == 'y'

async def first_action(websocket):
    await websocket.send(json.dumps(action_list_all_red))


async def send(websocket):
    global action_list
    while True:
        await websocket.send(json.dumps(action_list))
        await asyncio.sleep(0.1)
            
async def receive(websocket):
    while True:
        received_raw = await websocket.recv()
        if received_raw is not None:
            if not send_byte_arrays:
                await control(received_raw.replace('[','').replace(']','').replace('"','').split(','))
            else:
                await control(received_raw.decode("utf-8").replace('[','').replace(']','').replace('"','').split(','))

async def manage_lights(traffic_light,color):
    global action_list
    temp_incompatible_list = traffic_light.incompatible_traffic_lights

    def change_color(light_obj,new_color):
        light_obj_green = {"light":light_obj["light"], "status":"green", "timer": 0}
        light_obj_orange = {"light":light_obj["light"], "status":"orange", "timer": 0}
        light_obj_red = {"light":light_obj["light"], "status":"red", "timer": 0}

        if(new_color == "orange" and light_obj_green in action_list):
            action_list.remove(light_obj_green)
            action_list.append(light_obj_orange)
        if(new_color == "red" and light_obj_orange in action_list):
            action_list.remove(light_obj_orange)
            action_list.append(light_obj_red)
            if(light_obj["light"] in waiting_priority_list):
                waiting_priority_list.remove(light_obj["light"])

    for light in traffic_lights:
        if (light not in temp_incompatible_list):
            temp_incompatible_list = list(set(temp_incompatible_list + traffic_lights[light].incompatible_traffic_lights))
            light_obj_green = {"light":light, "status":"green", "timer": 0}
            light_obj_orange = {"light":light, "status":"orange", "timer": 0}
            light_obj_red = {"light":light, "status":"red", "timer": 0}

            if (light_obj_green not in action_list and light_obj_orange not in action_list):
                if(light_obj_red in action_list):
                    action_list.remove(light_obj_red)

                action_list.append(light_obj_green)

                light_timer = Timer(green_timer,change_color,[light_obj_green,"orange"])
                light_timer.start()

                light_timer = Timer(green_timer+orange_timer,change_color,[light_obj_orange,"red"])
                light_timer.start()

async def control(received_list):
    global waiting_priority_list, action_list

    for light in received_list:
        if light not in waiting_priority_list:
            if light == 'D1' or light == 'F1' or light == 'F2':
                waiting_priority_list.insert(1,light)
            else:
                waiting_priority_list.append(light)

    print(waiting_priority_list)
    await manage_lights(traffic_lights[waiting_priority_list[0]],"green")

async def fill_traffic_lights():
    global traffic_lights
    traffic_lights = {
        "A1": TrafficLight("A1", ["A5", "A7", "B1", "B3", "C1.1", "C3.2", "D1"]),
        "A2": TrafficLight("A2", ["F1", "F2", "A5", "A7", "A8", "A9", "A10", "B1", "C1.1", "D1", "E1"]),
        "A3": TrafficLight("A3", ["A5", "A7", "A6", "A9", "A10", "B1", "C1.1", "C2.2", "D1", "B2"]),
        "A4": TrafficLight("A4", ["A6", "A10", "B1", "B2", "C1.2", "C2.1", "D1"]),
        "A5": TrafficLight("A5", ["A1", "A2", "A3", "A6", "A7", "A10", "B2", "B3", "C3.2", "C2.1", "D1"]),
        "A6": TrafficLight("A6", ["F1", "F2", "A3", "A4", "A5", "A9", "A10", "B1", "B2", "C1.2", "C2.2", "D1", "E1"]),
        "A7": TrafficLight("A7", ["F1", "F2", "A1", "A2", "A3", "A5", "A9", "A10", "B3", "C3.2", "D1", "E1"]),
        "A8": TrafficLight("A8", ["F1", "F2", "A2", "B3", "C3.1", "E1"]),
        "A9": TrafficLight("A9", ["A2", "A3", "A6", "A7", "B2", "B3", "C2.2", "C3.1"]),
        "A10": TrafficLight("A10", ["A2", "A3", "A4", "A5", "A6", "A7", "B1", "B3", "C1.2", "C3.1", "D1"]),
        "B1": TrafficLight("B1", ["A1", "A2", "A3", "A10", "A4", "A6"]),
        "B2": TrafficLight("B2", ["A9", "A3", "A6", "A5", "A4", "D1"]),
        "B3": TrafficLight("B3", ["A8", "A9", "A10", "A1", "A7", "D1", "A5"]),
        "C1.1": TrafficLight("C1.1", ["A1", "A2", "A3"]),
        "C1.2": TrafficLight("C1.2", ["A6", "A4", "A10"]),
        "C2.1": TrafficLight("C2.1", ["D1", "A5", "A4"]),
        "C2.2": TrafficLight("C2.2", ["A6", "A3", "A9"]),
        "C3.1": TrafficLight("C3.1", ["A10", "A9", "A8"]),
        "C3.2": TrafficLight("C3.2", ["A1", "A5", "A7", "D1"]),
        "D1": TrafficLight("D1", ["B2", "B3", "C3.2","C2.1", "A4", "A5", "A7", "A6", "A1", "A2", "A3", "A10"], True),
        "F1": TrafficLight("F1", ["A7", "A6", "A8", "A2", "F2"], True),
        "F2": TrafficLight("F2", ["A7", "A6", "A8", "A2", "F1"], True)
    }

class TrafficLight:
    def __init__(self, name, incompatible_traffic_lights, prioritized=False, status="red"):
        self.name = name
        self.incompatible_traffic_lights = incompatible_traffic_lights
        self.prioritized = prioritized
        self.waiting_time = 0
        self.status = status

async def init(websocket,path):
    print("Started!")

    global waiting_priority_list,action_list
    waiting_priority_list = []
    action_list = []

    await fill_traffic_lights()
    await first_action(websocket)

    send_task = asyncio.ensure_future(send(websocket))
    receive_task = asyncio.ensure_future(receive(websocket))
    await asyncio.gather(send_task, receive_task)

run_server = websockets.serve(init, port=port)

asyncio.get_event_loop().run_until_complete(run_server)
asyncio.get_event_loop().run_forever()

