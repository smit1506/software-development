let controller_ip = prompt("Insert the IP address","localhost:6969");
let send_byte_array = confirm("Press OK to send and retrieve byte arrays or Cancel to use strings.");

if ( WEBGL.isWebGLAvailable() === false ) {
    document.body.appendChild( WEBGL.getWebGLErrorMessage() );
}

var ws, canvas, camera, controls, scene, renderer, frame = 0, fps = 30, previous_seconds = new Date().getTime() / 1000, fps_counter = 0;
let allTrafficLightsRed = [{"light": "F1", "status":"red", "timer": 0},{"light": "F2", "status":"red", "timer": 0},{"light": "E1", "status":"green", "timer": 0},{"light": "D1", "status":"red", "timer": 0},{"light":"A1","status":"red","timer":0},{"light":"A2","status":"red","timer":0},{"light":"A3","status":"red","timer":0},{"light":"A4","status":"red","timer":0},{"light":"A5","status":"red","timer":0},{"light":"A6","status":"red","timer":0},{"light":"A7","status":"red","timer":0},{"light":"A8","status":"red","timer":0},{"light":"A9","status":"red","timer":0},{"light":"A10","status":"red","timer":0},{"light":"B1","status":"red","timer":0},{"light":"B2","status":"red","timer":0},{"light":"B3","status":"red","timer":0},{"light":"C1.1","status":"red","timer":0},{"light":"C1.2","status":"red","timer":0},{"light":"C2.1","status":"red","timer":0},{"light":"C2.2","status":"red","timer":0},{"light":"C3.1","status":"red","timer":0},{"light":"C3.2","status":"red","timer":0}];
const fps_cap = 35;

init();
//render(); // remove when using next line for animation loop (requestAnimationFrame)
animate();

function init() {
    //ws = new WebSocket("ws://localhost:8080");
    ws = new WebSocket("ws://"+controller_ip)
    function onMessage(event) {
        //console.log("received " + event.data);
        let json_string = event.data;
        if (send_byte_array){
            json_string = String.fromCharCode.apply(null, event.data);
        }
        let edited_event_data = JSON.parse(json_string);

        let prepare_exception_for_trains = JSON.parse(event.data);
        let e1_object_red = {"light": "E1", "status":"red", "timer": 0}; 
        let e1_object_green = {"light": "E1", "status":"green", "timer": 0};

        for(let i = 0; i < prepare_exception_for_trains.length; i++) {

            if((prepare_exception_for_trains[i]["light"] == "F1" || prepare_exception_for_trains[i]["light"] == "F2") && prepare_exception_for_trains[i]["status"] == "red") {
                if(edited_event_data.includes(e1_object_green)) break;
                edited_event_data.push(e1_object_green);
            }
            if((prepare_exception_for_trains[i]["light"] == "F1" || prepare_exception_for_trains[i]["light"] == "F2") && prepare_exception_for_trains[i]["status"] == "green") {
                if(edited_event_data.includes(e1_object_red)) break;
                edited_event_data.push(e1_object_red);
            }
        }


        control_traffic_lights(edited_event_data);
        

        let output = Object.entries(traffic_lights).filter(e=>e[1].state==="red"&&e[1].current_traffic>0).map(e=>e[0]);
        if (output.length > 0) {
            ws.send(JSON.stringify(output));
            // if (!send_byte_array){
            // }
            // else {
            //     let utf8 = unescape(encodeURIComponent(str));

            //     let arr = [];
            //     for (let i = 0; i < utf8.length; i++) {
            //         arr.push(utf8.charCodeAt(i));
            //     }

            //     ws.send(JSON.stringify(arr));

            // }
            //console.log(JSON.stringify(output));
        }
    }
    function onOpen(event) {
        control_traffic_lights(allTrafficLightsRed);
    }
    function onError(event) {
        console.error("WebSocket error observed:", event);
    }
    function onClose(event) {
        console.error("WebSocket is closed now:", event);
        setTimeout(()=>{
                ws = new WebSocket("ws://"+controller_ip);
                ws.addEventListener('message', onMessage);
                ws.addEventListener('open', onOpen);
                ws.addEventListener('close', onClose);
                ws.addEventListener('error', onError);
        }, 3000);
        control_traffic_lights(allTrafficLightsRed);
    }
    ws.addEventListener('message', onMessage);
    ws.addEventListener('open', onOpen);
    ws.addEventListener('error', onError);
    ws.addEventListener('close', onClose);

    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xfcfcfc );
    scene.fog = new THREE.FogExp2( 0xffffff, 0.00075 );

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );

    canvas = renderer.domElement;
    document.body.appendChild(canvas);

    camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 5000 );
    camera.position.set( -700, 600, 700 );

    document.getElementsByTagName("canvas")[0].addEventListener("click",getClicked3DPoint);

    // controls
    controls = new THREE.OrbitControls( camera, renderer.domElement );

    //controls.addEventListener( 'change', render ); // call this only in static scenes (i.e., if there is no animation loop)
    controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
    controls.dampingFactor = 0.3;
    controls.screenSpacePanning = false;
    controls.minDistance = 20;
    controls.maxDistance = 750;
    controls.maxPolarAngle = Math.PI / 2.1;

    // loading manager
    var loadingManager = new THREE.LoadingManager( function() {
        scene.add( world );
        world.scale.x = 2;
        world.scale.y = 2;
        world.scale.z = 2;

        world.name = 'world';

        world.position.x = -300;
        world.position.z = 300;

    });

    var loader = new THREE.ColladaLoader( loadingManager );
    var textureLoader = new THREE.TextureLoader();
    var texture = textureLoader.load('models/kruispunt.jpg');

    loader.load( 'models/model.dae', function ( collada ) {
        world = collada.scene;

        collada.scene.traverse(function (node) {
            if (node.isMesh) node.material.map = texture;
        });
    });

    // lights
    var light = new THREE.SpotLight( 0xfcfcfc, 0.9, 0, Math.PI / 2 );
    light.position.set( 0, 3500, 0 );
    light.target.position.set( 0, 0, 0 );
    
    scene.add( light );

    var light = new THREE.AmbientLight( 0xffffff );
    scene.add( light );

    window.addEventListener( 'resize', onWindowResize, false );

    spawn_car_init();
    spawn_cyclist_init();
    spawn_pedestrian_init();

    show_stop_waypoints(waypoints_indexof_lights_cars,waypoints_cars,"car");
    show_stop_waypoints(waypoints_indexof_lights_cyclists,waypoints_cyclists,"cyclist");
    show_stop_waypoints(waypoints_indexof_lights_pedestrians,waypoints_pedestrians,"pedestrian");

    control_traffic_lights(allTrafficLightsRed);
    setInterval(function(){ train(); }, 40000);
}

function getClicked3DPoint(evt) {
    evt.preventDefault();
    let rayCaster = new THREE.Raycaster();
    let mousePosition = new THREE.Vector2();

    mousePosition.x = ((evt.clientX) / canvas.width) * 2 - 1;
    mousePosition.y = -((evt.clientY) / canvas.height) * 2 + 1;

    rayCaster.setFromCamera(mousePosition, camera);
    let intersects = rayCaster.intersectObjects(scene.children, true);

    if (intersects.length > 0)
        waypoint_setter.push([Math.round(intersects[0].point.x),Math.round(intersects[0].point.z)]);

    if (intersects.length > 0 && (intersects[0].object.geometry.type == "SphereGeometry" || intersects[0].object.type == "car"))
        console.log(intersects[0].object);
    //console.log(waypoint_setter);
};

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

function animate() {
    requestAnimationFrame( animate );
    animate_traffic();
    controls.update(); // only required if controls.enableDamping = true, or if controls.autoRotate = true
    render();
    frame++;
    if(frame % 70 == 0 && traffic.length < 75) fill_traffic();

    let seconds = new Date().getTime() / 1000;
    fps_counter++;
    if(seconds - previous_seconds >= 1){
        fps = fps_counter;
        fps_counter = 0;
        previous_seconds = seconds;
        console.log(fps);
        speed_multiplier = (fps_cap/fps);
        max_speed = 1.5*speed_multiplier
        decel_speed = 0.0415*speed_multiplier;
        accel_speed = 0.02*speed_multiplier;
    }

}

function train() {
    //traffic_lights["E1"].current_traffic = 1;
    let train_spawn_point = 13;
    if(Math.round(Math.random()) >= 0.5) train_spawn_point = 14;

    if(train_spawn_point == 13) traffic_lights["F1"].current_traffic = 1;
    if(train_spawn_point == 14) traffic_lights["F2"].current_traffic = 1;

    setTimeout(() => {
        traffic_lights["F1"].current_traffic = 0;
        traffic_lights["F2"].current_traffic = 0;
        spawn_car(train_spawn_point);
    }, 3000);
}

function render() {
    renderer.render( scene, camera );
}

		